const getStudents = async () => {
    try {
        const response = await fetch("https://d8ba-152-57-209-228.ngrok.io/students", {

            method: "GET",
            headers: new Headers({
                "ngrok-skip-browser-warning": "1234",
            })

        });
        const data = await response.json();
        return data;


    } catch (e) {
        console.log(e);
    }
}

const displayStudents = (data) => {
    const tbody = document.getElementById("tbody");
    console.log(data);


    for (let student of data) {
        const tr = document.createElement("tr");

        tbody.append(tr);

        const { firstName, middleName, lastName, studentId, standard, dateOfBirth, telephoneNumber, address, grade } = student;



        const firstNameTd = document.createElement("td");

        firstNameTd.innerText = firstName;

        tr.appendChild(firstNameTd);

        const middleNameTd = document.createElement("td");
        middleNameTd.innerText = middleName;
        tr.appendChild(middleNameTd);


        const lastNameTd = document.createElement("td");
        lastNameTd.innerText = lastName;
        tr.appendChild(lastNameTd);

        const studentIdTd = document.createElement("td");

        studentIdTd.innerText = studentId;
        tr.appendChild(studentIdTd);

        const standardTd = document.createElement("td");

        standardTd.innerText = standard;
        tr.appendChild(standardTd);

        const dateOfBirthTd = document.createElement("td");

        dateOfBirthTd.innerText = dateOfBirth;
        tr.appendChild(dateOfBirthTd);

        const telephoneTd = document.createElement("td");

        telephoneTd.innerText = telephoneNumber;
        tr.appendChild(telephoneTd);

        const addressTd = document.createElement("td");

        addressTd.innerText = address;
        tr.appendChild(addressTd);

        const gradeTd = document.createElement("td");

        gradeTd.innerText = grade;
        tr.appendChild(gradeTd);


        const td1 = document.createElement("td");
        const edit = document.createElement("button");
        edit.setAttribute("id", "edit");
        edit.setAttribute("class", "edit-button");
        let node11 = document.createTextNode("edit");
        edit.appendChild(node11);
        td1.appendChild(edit);
        tr.appendChild(td1);
        edit.addEventListener("click", () => updateDetails(student));

        const del = document.createElement("button");
        del.setAttribute("id", "delete");
        del.setAttribute("class", "delete-button");
        let node21 = document.createTextNode("delete");
        del.appendChild(node21)
        td1.appendChild(del);
        tr.appendChild(td1);

        del.addEventListener("click", () => deleteDetails(studentId));




    }

}




const main = async () => {
    try {
        const users = await getStudents();
        displayStudents(users);
    } catch (e) {
        console.log(e);
    }
}


main();







