

const updateSection = document.getElementById("update-form");
function updateDetails(student) {


    const { firstName, middleName, lastName, studentId, standard, dateOfBirth, telephoneNumber, address, grade } = student;
    const form = document.createElement("form");


    const firstname = document.createElement("label");
    firstname.setAttribute("for", "firstname");
    firstname.innerHTML = "Name:"
    const firstnameInput = document.createElement("input");
    firstnameInput.setAttribute("type", "text");
    firstnameInput.setAttribute("id", "firstname");
    firstnameInput.classList.add("input");

    firstnameInput.value = `${firstName}`;

    const middlename = document.createElement("label");
    middlename.setAttribute("for", "middlename");
    middlename.innerHTML = "Name:"
    const middlenameInput = document.createElement("input");
    middlenameInput.setAttribute("type", "text");
    middlenameInput.setAttribute("id", "middlename");
    middlenameInput.value = `${middleName}`;
    middlenameInput.classList.add("input");



    const lastname = document.createElement("label");
    lastname.setAttribute("for", "lastname");
    lastname.innerHTML = "Name:"
    const lastnameInput = document.createElement("input");
    lastnameInput.setAttribute("type", "text");
    lastnameInput.setAttribute("id", "lastname");
    lastnameInput.value = `${lastName}`;
    lastnameInput.classList.add("input");

    const rollNumber = document.createElement("label");
    rollNumber.setAttribute("for", "rollNumber");
    rollNumber.innerHTML = "Roll Number:"
    const rollInput = document.createElement("input");
    rollInput.setAttribute("type", "text");
    rollInput.setAttribute("id", "rollNumber");
    rollInput.value = `${studentId}`;
    rollInput.classList.add("input");

    const std = document.createElement("label");
    std.setAttribute("for", "std");
    std.innerHTML = "STD:"
    const stdInput = document.createElement("input");
    stdInput.setAttribute("type", "text");
    stdInput.setAttribute("id", "std");
    stdInput.value = `${standard}`;
    stdInput.classList.add("input");




    const age = document.createElement("label");
    age.setAttribute("for", "dob");
    age.innerHTML = "Date of Birth:"
    const ageInput = document.createElement("input");
    ageInput.setAttribute("type", "date");
    ageInput.setAttribute("id", "dob");
    ageInput.value = `${dateOfBirth}`;
    ageInput.classList.add("input");

    const telephone = document.createElement("label");
    telephone.setAttribute("for", "telephone");
    telephone.innerHTML = "Telephone Number:"
    const telephoneInput = document.createElement("input");
    telephoneInput.setAttribute("type", "number");
    telephoneInput.setAttribute("id", "telephone");
    telephoneInput.value = `${telephoneNumber}`;
    telephoneInput.classList.add("input");

    const addressLabel = document.createElement("label");
    addressLabel.setAttribute("for", "address");
    addressLabel.innerHTML = "Address:"
    const addressInput = document.createElement("input");
    addressInput.setAttribute("type", "text");
    addressInput.setAttribute("id", "address");
    addressInput.value = `${address}`;
    addressInput.classList.add("input");

    const gradeLabel = document.createElement("label");
    gradeLabel.setAttribute("for", "grade");
    gradeLabel.innerHTML = "Grade:"
    const gradeInput = document.createElement("input");
    gradeInput.setAttribute("type", "text");
    gradeInput.setAttribute("id", "grade");
    gradeInput.value = `${grade}`;
    gradeInput.classList.add("input");

    const submitButton = document.createElement("button");
    submitButton.innerHTML = "Submit";
    submitButton.classList.add("submit");
    form.appendChild(firstname);
    form.appendChild(firstnameInput);
    form.appendChild(middlename);
    form.appendChild(middlenameInput);
    form.appendChild(lastname);
    form.appendChild(lastnameInput);
    form.appendChild(rollNumber);
    form.appendChild(rollInput);
    form.appendChild(std);
    form.appendChild(stdInput);
    form.appendChild(age);
    form.appendChild(ageInput);
    form.appendChild(telephone);
    form.appendChild(telephoneInput);
    form.appendChild(addressLabel);
    form.appendChild(addressInput);
    form.appendChild(gradeLabel);
    form.appendChild(gradeInput);
    form.appendChild(submitButton);
    updateSection.appendChild(form);




    if (form.addEventListener("submit", async () => {
        let firstName = document.getElementById('firstname').value;
        let middleName = document.getElementById('middlename').value;
        let lastName = document.getElementById('lastname').value;
        let studentId = document.getElementById('rollNumber').value;
        let standard = document.getElementById('std').value;

        let dateOfBirth = document.getElementById('dob').value;
        let telephoneNumber = document.getElementById('telephone').value;
        let address = document.getElementById('address').value;
        let grade = document.getElementById('grade').value;
        try {
            fetch("https://d8ba-152-57-209-228.ngrok.io/update", {
                method: "PUT",
                body: JSON.stringify({
                    "firstName": firstName,
                    "middleName": middleName,
                    "lastName": lastName,
                    "studentId": studentId,
                    "standard": standard,
                    "dateOfBirth": dateOfBirth,
                    "telephoneNumber": telephoneNumber,
                    "address": address,
                    "grade": grade
                }),
                headers: { 'Content-type': 'application/json; charset=UTF-8' }
            })
        }
        catch (e) {
            console.log(e)
        }







    }));

}








