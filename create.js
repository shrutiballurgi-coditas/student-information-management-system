const createButton = document.getElementById("button");
const sectionContainer=document.getElementById("form");
const tbody=document.getElementById("tbody");
createButton.addEventListener("click",()=>{
    
    const form=document.createElement("form");

    
    const firstname=document.createElement("label");
    firstname.setAttribute("for","firstname");
    firstname.innerHTML="Firstname:"
    const firstnameInput=document.createElement("input");
    firstnameInput.setAttribute("type", "text");
    firstnameInput.setAttribute("id", "firstname");
    firstnameInput.setAttribute("placeholder", "Shruti");
    firstnameInput.classList.add("input");

    const middlename=document.createElement("label");
    middlename.setAttribute("for","middlename");
    middlename.innerHTML="Middlename:"
    const middlenameInput=document.createElement("input");
    middlenameInput.setAttribute("type", "text");
    middlenameInput.setAttribute("id", "middlename");
    middlenameInput.setAttribute("placeholder", "Somashekhar");
    middlenameInput.classList.add("input");

    const lastname=document.createElement("label");
    lastname.setAttribute("for","lastname");
    lastname.innerHTML="Lastname:"
    const lastnameInput=document.createElement("input");
    lastnameInput.setAttribute("type", "text");
    lastnameInput.setAttribute("id", "lastname");
    lastnameInput.setAttribute("placeholder", "Ballurgi");
    lastnameInput.classList.add("input");

    const rollNumber=document.createElement("label");
    rollNumber.setAttribute("for","rollNumber");
    rollNumber.innerHTML="Roll Number:"
    const rollInput=document.createElement("input");
    rollInput.setAttribute("type", "number");
    rollInput.setAttribute("id", "rollNumber");
    rollInput.setAttribute("placeholder", "1");
    rollInput.classList.add("input");
    
    const std=document.createElement("label");
    std.setAttribute("for","std");
    std.innerHTML="STD:"
    const stdInput=document.createElement("input");
    stdInput.setAttribute("type", "number");
    stdInput.setAttribute("id", "std");
    stdInput.setAttribute("placeholder", "10");
    stdInput.classList.add("input");
    
    
    
    const age=document.createElement("label");
    age.setAttribute("for","dob");
    age.innerHTML="Date of Birth:"
    const ageInput=document.createElement("input");
    ageInput.setAttribute("type", "date");
    ageInput.setAttribute("id", "dob");
    ageInput.setAttribute("placeholder", "10-10-2000");
    ageInput.classList.add("input");
    
    const telephone=document.createElement("label");
    telephone.setAttribute("for","telephone");
    telephone.innerHTML="Telephone Number:"
    const telephoneInput=document.createElement("input");
    telephoneInput.setAttribute("type", "number");
    telephoneInput.setAttribute("id", "telephone");
    telephoneInput.setAttribute("placeholder", "9887654322");
    telephoneInput.classList.add("input");

    const address=document.createElement("label");
    address.setAttribute("for","address");
    address.innerHTML="Address:"
    const addressInput=document.createElement("input");
    addressInput.setAttribute("type", "text");
    addressInput.setAttribute("id", "address");
    addressInput.setAttribute("placeholder", "Viman nagar-Pune");
    addressInput.classList.add("input");
    
    const grade=document.createElement("label");
    grade.setAttribute("for","grade");
    grade.innerHTML="Grade:"
    const gradeInput=document.createElement("input");
    gradeInput.setAttribute("type", "text");
    gradeInput.setAttribute("id", "grade");
    gradeInput.setAttribute("placeholder", "A");
    gradeInput.classList.add("input");

    const submitButton=document.createElement("button");
    submitButton.innerHTML="Submit";
    submitButton.classList.add("submit");
    form.appendChild(firstname);
    form.appendChild(firstnameInput);
    form.appendChild(middlename);
    form.appendChild(middlenameInput);
    form.appendChild(lastname);
    form.appendChild(lastnameInput);
    form.appendChild(rollNumber);
    form.appendChild(rollInput);
    form.appendChild(std);
    form.appendChild(stdInput);
    form.appendChild(age);
    form.appendChild(ageInput);
    form.appendChild(telephone);
    form.appendChild(telephoneInput);
    form.appendChild(address);
    form.appendChild(addressInput);
    form.appendChild(grade);
    form.appendChild(gradeInput);
    form.appendChild(submitButton);
    sectionContainer.appendChild(form);


    if(form.addEventListener("submit", async ()=>{
        let firstName=document.getElementById('firstname').value;
        let middleName=document.getElementById('middlename').value;
        let lastName=document.getElementById('lastname').value;
        let studentId=document.getElementById('rollNumber').value;
        let standard=document.getElementById('std').value;
        
        let dateOfBirth=document.getElementById('dob').value;
        let telephoneNumber=document.getElementById('telephone').value;
        let address=document.getElementById('address').value;
        let grade=document.getElementById('grade').value;
        try{
            fetch("https://d8ba-152-57-209-228.ngrok.io/register",{
                method:"POST",
                body: JSON.stringify({
                    "firstName":firstName,
                    "middleName":middleName,
                    "lastName":lastName,
                    "studentId":studentId,
                    "standard":standard,
                    "dateOfBirth":dateOfBirth,
                    "telephoneNumber":telephoneNumber,
                    "address":address,
                    "grade":grade
                }),
                headers: {'Content-type': 'application/json; charset=UTF-8'}
            })
            }
        catch(e){
            console.log(e)
        }

    
}));
})

getStudents();













