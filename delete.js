function deleteDetails(id) {
    console.log(id);
    const div=document.createElement("div");
    div.classList.add("delete-container");
    const alertStatement=document.createElement("p");
    document.body.append(div);
    alertStatement.innerText="Are you Sure to delete?";
    div.appendChild(alertStatement);
    const yesButton=document.createElement("button");
    yesButton.innerText="Yes";
    yesButton.classList.add("yesButton");
    div.appendChild(yesButton);
    const noButton=document.createElement("button");
    noButton.innerText="No";
    noButton.classList.add("noButton");
    div.appendChild(noButton);
    console.log(id);
    yesButton.addEventListener("click",async ()=>{
    
    try {
         fetch(`https://d8ba-152-57-209-228.ngrok.io/delete/${id}`, {
            method: "DELETE",
            headers:{
                "ngrok-skip-browser-warning": "1234",
                'Content-type': 'application/json; charset=UTF-8'}

        });
    } catch (err) {
        console.log(err);
    }

    noButton.addEventListener("click",()=>{
        div.style.display="none";
    }
    )
})}